import sys
import math
import random
import openpyxl as excel
from typing import List
from typing import Tuple
from typing import Dict
from datetime import date
from datetime import datetime
from datetime import timedelta




'''
    Made by ElMrTor
    Why did I make this?? -> cuz this is one of the most retarded assigments I've seen.
    Code needs refactoring.
'''



class Utils():

    DAY_PROP = {'Saturday': (5), 'Sunday': (8), 'Monday': (11), 'Tuesday': (14), 'Wednesday': (17), 'Thursday': (20), 'Friday': (23)}
    ROW_START_POSITION = 3


    @staticmethod
    def get_time_duration(duration: float) -> timedelta:
        minutes, hours = math.modf(duration)
        hours = int(hours)
        if len(str(minutes)) == 3:
            minutes = int('{:0<4}'.format(minutes)[2:4])            
        else:
            minutes = int(str(minutes)[2:4])
        return timedelta(hours=hours, minutes=minutes)

    @staticmethod
    def generate_event_duration(min: float, max: float) -> timedelta:
        time_gen = random.Random()
        time = time_gen.uniform(min, max)
        return Utils.get_time_duration(time)

    @staticmethod
    def pad_time_uniform(time: timedelta) -> str:
        if len(str(time)) != 8:
            return '0'.join(str(time))
        else:
            return str(time)

    @staticmethod
    def load_resource_file() -> Tuple[Dict, List]:
        random_activities = Utils.__load_random_activities()
        day_act = Utils.__load_day_info()
        return (random_activities, day_act)


    @staticmethod
    def __load_random_activities() -> Dict:
        xl_wb: excel.workbook.Workbook = excel.load_workbook('res_file.xlsx')
        activities_sheet: excel.worksheet.worksheet.Worksheet = xl_wb.active
        act_dict = {}

        activity_names = []
        for row in activities_sheet.iter_rows(3, 100, 1, 1):
            for cell in row:
                if cell.value:
                    activity_names.append(cell.value)

        activity_durations = []
        for row in activities_sheet.iter_rows(3, 100, 2, 3):
            min_dur, max_dur = row
            # print(f'{min_dur.value} -- {max_dur.value}')
            if min_dur.value and max_dur.value:
                activity_durations.append((float(min_dur.value), float(max_dur.value)))
            
        for idx, entry in enumerate(activity_names):
            act_dict[entry] = list(activity_durations[idx])
        # print(act_dict)
        return act_dict


    @staticmethod
    def get_activity_comments() -> Dict:
        xl_wb: excel.workbook.Workbook = excel.load_workbook('res_file.xlsx')
        current_sheet: excel.worksheet.worksheet.Worksheet = xl_wb.active
        activity_names = []
        for row in current_sheet.iter_rows(Utils.ROW_START_POSITION, 100, 1, 1):
            cell: excel.cell.Cell
            for cell in row:
                if cell.value:
                    activity_names.append(cell.value)
                
        activity_comments = []
        for row in current_sheet.iter_rows(Utils.ROW_START_POSITION, 100, 4, 4):
            for cell in row:
                if cell.value:
                    act_comms = cell.value.split(',')
                    for idx, cmm in enumerate(act_comms):
                        act_comms[idx] = cmm.strip()
                    activity_comments.append(act_comms)
                else:
                    activity_comments.append([]) # Compensate for activities with no comments in them.

        activity_dict = {}
        for activity, comments in zip(activity_names, activity_comments):
            activity_dict[activity] = comments
        return activity_dict

            

    @staticmethod
    def __load_day_info() -> List:
        xl_wb: excel.workbook.Workbook = excel.load_workbook('res_file.xlsx')
        activities_sheet: excel.worksheet.worksheet.Worksheet = xl_wb.active
        day_list = []
        for day in Utils.DAY_PROP.keys():
            day_list.append(Utils.__extract_specific_day_info(activities_sheet, day))
        return day_list

               
        
    @staticmethod
    def __extract_specific_day_info(day_sheet: excel.worksheet.worksheet.Worksheet, current_day: str) -> Dict:

        stationary_activities = {}

        activity_names = []
        for row in day_sheet.iter_rows(3, 100, Utils.DAY_PROP[current_day], Utils.DAY_PROP[current_day]):
            for cell in row:
                if cell.value:
                    activity_names.append(cell.value)

        activity_start_duration = []
        for row in day_sheet.iter_rows(3, 100, Utils.DAY_PROP[current_day] + 1, Utils.DAY_PROP[current_day] + 2):
            start_time, duration = row
            if start_time.value and duration.value:
                activity_start_duration.append((float(start_time.value), float(duration.value)))

        
        for idx, entry in enumerate(activity_names):
            stationary_activities[entry] = activity_start_duration[idx]
        
        return stationary_activities
   



class Activity:

    def __init__(self, start_time, duration, name) -> None:
        self.activity_name: str = name
        if isinstance(duration, float):
            self.activity_duration: timedelta = Utils.get_time_duration(duration)
        else:
            self.activity_duration: timedelta = duration
        if isinstance(start_time, float):
            self.start_time: timedelta = Utils.get_time_duration(start_time)
        else:
            self.start_time: timedelta = start_time
        self.end_time: timedelta = self.start_time + self.activity_duration
        
    
    def __str__(self) -> str:
        ''' Yea this method is weird so idk. '''
        return str(self.start_time) # It has a str method

    def __lt__(self, other: object) -> bool:
        return self.start_time < other.start_time

    def __gt__(self, other: object) -> bool:
        return self.start_time > other.start_time

    def __eq__(self, other: object) -> bool:
        return self.start_time == other.start_time

    def __ge__(self, other: object) -> bool:
        return self.start_time >= other.start_time

    def __le__(self, other: object) -> bool:
        return self.start_time <= other.start_time

    def __eq__(self, other: object) -> bool:
        return self.activity_name == other.activity_name


    

class Day:

    def __init__(self):
        self.current_time: timedelta = timedelta(days=0)
        self.stationary_activities: List[Activity] = []
        self.day_activities: List[Activity] = []

    
    def __str__(self) -> str:
        d_str = '\n'
        for activity in self.day_activities:
            print(f'ActivityName -> {activity.activity_name} : StartTime -> {activity.start_time} : Duration -> {activity.activity_duration} : EndTime -> {activity.end_time}\n')
        return d_str

    def add_stationary_activity(self, activity: Activity) -> None:
        self.stationary_activities.append(activity)
        self.__sort_stationary_activities()
        
    
    def __adjust_time_after_add_activity(self) -> None:
        self.current_time = self.day_activities[-1].end_time

    def add_day_activity(self, activity: Activity) -> None:
        self.day_activities.append(activity)
        self.__adjust_time_after_add_activity()

    def __sort_stationary_activities(self) -> None:
        self.stationary_activities.sort()

    def set_hour_start(self, start: timedelta) -> None:
        self.current_time = start

    

class ActivityGenerator:

    NO_TIME = timedelta(days=0)

    IMPORTANT_ACTIVITIES = {
        'Sleep': [3.00, 5.00],
        'Eat': [0.30, 1.30], # Not being used at the moment, maybe for another time when I'm motivated.
        'Commute': [0.25, 1.15] # Not being used at the moment, maybe for another time when I'm motivated.
    }    

    random_activities = { 
    }

    def __init__(self, activity_list: List[Activity]):
        initial_act = Activity(timedelta(days=0), ActivityGenerator.generate_initial_sleep(3.0, 5.0), 'Sleep')
        self.current_day = Day()
        self.current_day.add_day_activity(initial_act)
        self.current_day.set_hour_start(initial_act.end_time) 
        act: Activity
        for act in activity_list:
            self.current_day.add_stationary_activity(act)
            act.activity_duration
            

    def fill_day_with_activities(self) -> None:
        act_size: int = len(self.current_day.stationary_activities)
        for i in range(act_size):
            self.generate_activity_block()

        self.add_final_activity()
        self.generate_activity_block()


    def get_time_block_duration(self) -> timedelta:
        if len(self.current_day.stationary_activities) < 1:
            print('There are no activities to get a time block from.')
            sys.exit(1)
        else:
            return self.current_day.stationary_activities[0].start_time - self.current_day.current_time  


    def generate_activity_block(self) -> None: # Pensando en horas y segundos
        #Initial case of what to do with initial event at start of day
        time_block: timedelta = self.get_time_block_duration()

        # print(f'Timeblock has a duration of --> {str(time_block)}')

        if time_block != ActivityGenerator.NO_TIME:
            while(time_block != ActivityGenerator.NO_TIME):
                rand_activity, time_block_left = self.generate_random_activity(time_block)
                self.current_day.add_day_activity(rand_activity)                
                time_block = time_block_left
                
            self.current_day.add_day_activity(self.current_day.stationary_activities.pop(0))
            
        else:
            print('Dropped activity.\n')
        
        
    def generate_random_activity(self, time_block_duration: timedelta) -> Tuple[Activity, timedelta]:
        picker: random.Random = random.Random()
        activity_name: str = picker.choice(list(ActivityGenerator.random_activities.keys()))
        min_duration, max_duration = ActivityGenerator.random_activities[activity_name]
        act_duration: timedelta = Utils.generate_event_duration(min_duration, max_duration)
        if act_duration > time_block_duration:
            act_duration = time_block_duration
            return (Activity(self.current_day.current_time, act_duration, activity_name), timedelta(days=0))
        else:
            return (Activity(self.current_day.current_time, act_duration, activity_name), time_block_duration - act_duration)

    def add_final_activity(self):
        end_day: timedelta = Utils.get_time_duration(24.0)
        duration: timedelta = Utils.generate_event_duration(*ActivityGenerator.IMPORTANT_ACTIVITIES['Sleep'])
        final_sleep: Activity = Activity(end_day - duration, duration, 'Sleep')
        self.current_day.stationary_activities.append(final_sleep)

    def merge_adjacent_activities(self) -> None:
        '''
        Used after list of generated activities is created to merge same activities that were created one after the other by creating a new 
        activity with the duration of both activities and remove both of the old activities. 
        '''
        if len(self.current_day.day_activities) < 1:
            print('There are no activities to work with, will exit...')
            sys.exit(1)
        else:
            act_list = self.current_day.day_activities
            while self.repeating_adjacents():
                for i in range(len(self.current_day.day_activities) - 1):
                    if act_list[i].activity_name == act_list[i + 1].activity_name:
                        # print(f'Found activities with the same name -- {act_list[i].activity_name}')
                        start_time = act_list[i].start_time # Same as first activity, more readable.
                        new_duration = act_list[i].activity_duration + act_list[i + 1].activity_duration
                        act_list.insert(i, Activity(start_time, new_duration, act_list[i].activity_name)) # Insert before first activity
                        # Remove both activities used to create the new one.
                        act_list.pop(i + 1) # +1 because new activity was added before both activities.
                        act_list.pop(i + 1) # Stays at +1 because after removing every item gets moved to the left.
                        break
                    

    def repeating_adjacents(self) -> bool:
        ''' Return true if there is any two activities with the same name one after the other. '''
        size = len(self.current_day.day_activities) - 1
        act_list = self.current_day.day_activities
        for i in range(size):
            if act_list[i].activity_name == act_list[i + 1].activity_name:
                return True
        return False

    @staticmethod
    def set_class_random_activities(activity_dict: Dict) -> None:
        ''' Sets the class dictionary of activities. '''
        ActivityGenerator.random_activities = activity_dict

    @staticmethod
    def generate_initial_sleep(min_duration, max_duration) -> float:
        ''' Generate a float representating a time duration. '''
        time_gen = random.Random()
        return time_gen.uniform(min_duration, max_duration)

class WeekManager:

    def __init__(self):
        self.days = []
        print('*'*51)
        print('\nEnter start of the week at Saturday. e.g. 2020 2 15')
        self.start_date = datetime(int(input('Enter year\n')), int(input('Enter month number\n')), int(input('Enter day number\n')))        
        self.random_activities, self.act_per_days = Utils.load_resource_file()
        print('\n\n')


    def initialize_activity_generator_for_week(self) -> None:
        ''' Creates a ActivityGenerator to fill each day of the week with activities. '''
        generators = []
        
        for day in self.act_per_days:
            
            act_gen = ActivityGenerator(self.convert_dict_into_activities(day))
            
            act_gen.merge_adjacent_activities()
            generators.append(act_gen)
            self.days.append(act_gen.current_day)
            
        ActivityGenerator.set_class_random_activities(self.random_activities)
        for gen in generators:
            gen.fill_day_with_activities()
            gen.merge_adjacent_activities()
        

    def get_date_with_day(self) -> Tuple[datetime, Day]:
        ''' Returns a tuple with the datetime and day instances. '''
        date_per_day = []
        for i in range(len(Utils.DAY_PROP.keys())):
            date_per_day.append(self.start_date + timedelta(days = i))
        return zip(date_per_day, self.days)

    def convert_dict_into_activities(self, act_dict: Dict) -> List[Activity]:
        ''' Translates a dictionary to activity instances. '''
        act_list = []
        
        for act_name in act_dict.keys():
            start_time, duration_time = act_dict[act_name]                    
            # print(f'{start_time} ---- {duration_time}')
            act_list.append(Activity(start_time, duration_time, act_name))            

        return act_list


class ActivitySummaryContainer:

    ''' A class created to make it easier to organize the total times of each activity for summary, This class needs a lot of refactoring (confusing af). '''

    def __init__(self, current_week: List[Day], total_activity_time_dict: Dict):
        self.assigned_week = current_week # List of all the days in the week
        self.act_total_dict = total_activity_time_dict # Dictionary with activity name as keys and the activity as value
        self.current_activity_column = None # String used to indicate wich activity to read from day activity, used when calc total time for activity in one single day.
        

    def get_total_activity_time_per_day(self, current_day: Day) -> timedelta:
        ''' Returns the total activity duration of a single day according to the current_activity_column '''
        if self.current_activity_column is None:
            print('Current day activity is not assigned in ActivitySummaryContainer')
            raise Exception # Lol
        day_act_duration = timedelta()
        act: Activity
        for act in current_day.day_activities:
            if act.activity_name == self.current_activity_column:
                day_act_duration += act.activity_duration
        return day_act_duration

    def set_current_activity_column(self, activity_name: str) -> None:
        ''' Sets the string that indicates which activity is being looked for in a day. '''
        self.current_activity_column = activity_name

    def set_null_activity_column(self):
        ''' Resets the string activity to None. '''
        self.current_activity_column = None


class ExcelManager():

    #TODO Refactor using this variables in the class. ||| Someday, not today :)
    ROW_START_POSITION = 3
    DATE_COLUMN = 1
    START_TIME_COLUMN = 2
    ACTIVITY_TIME_COLUMN = 3
    END_TIME_COLUMN= 4
    ACTIVITY_NAME_COLUMN = 5
    COMMENT_COLUMN = 6


    #TODO finish fixing and adding this stuff. ||| Someday, not today :)
    SATURDAY_POSITION = None
    SUNDAY_POSITION = None
    MONDAY_POSITION = None
    TUESDAY_POSITION = None
    WEDNESDAY_POSITION = None
    THURSDAY_POSITION = None
    FRIDAY_POSITION = None
    

    TOTAL_WEEK_TIME = 168

    COLUMN_TITLES = [
        'Date',
        'Start of Activity',
        'Total Activity Time',
        'End of Activity',
        'Activity',
        'Comments'
    ]

    def __init__(self, filename: str, person_name: str):
        self.filename = filename
        self.xlfile = excel.Workbook()
        self.timelog_page = self.xlfile.active
        self.person_name = person_name
        self.picker = random.Random()
        self.current_row_position = ExcelManager.ROW_START_POSITION
        self.current_merge_row = ExcelManager.ROW_START_POSITION
        self.week_schedule = list(self.generate_and_obtain_week_schedule())
        # for date, day in self.week_schedule:
        #     print(f'This is the date {date} \n---- This is the day {day}')
        self.create_initial_layout()
        
        
        
    
    def generate_and_obtain_week_schedule(self) -> List[Tuple[datetime, Day]]:
        ''' Generates the week filled with activities with its corresponding date. '''
        week_mngr = WeekManager()
        week_mngr.initialize_activity_generator_for_week()
        return week_mngr.get_date_with_day()

    def create_initial_layout(self) -> None:
        ''' Writes the initial layout of the excel sheets. '''
        tm_page = self.timelog_page
        tm_page: excel.worksheet.worksheet.Worksheet
        tm_page.title = 'Time Log'

        # Set timelog title
        tm_page['A1'] = self.person_name
        tm_page.merge_cells(start_row=1, start_column=1, end_row=1, end_column=len(ExcelManager.COLUMN_TITLES)) # Sheets start at 1, not 0.


        # Set timelog column titles
        for idx, title in enumerate(ExcelManager.COLUMN_TITLES):
            tm_page.cell(2, idx + 1, title) # Row and columns start at 1, not 0.
        
    def save_excel_sheet(self) -> None:
        ''' Creates and/or writes the excel file. '''
        self.xlfile.save(self.filename)

    
    def write_schedule(self) -> None:
        ''' Writes the schedule to the excel sheet. '''
        for date, day in self.week_schedule:
            self.__write_day_to_file(date, day)
        self.__write_week_summary()
            


    def __write_day_to_file(self, a_date: date, current_day: Day) -> None:
        ''' Writes a single day to the excel sheet. '''
        tl_page: excel.worksheet.worksheet.Worksheet
        tl_page = self.timelog_page        
        total_act: int = len(current_day.day_activities)
        act_comm_dict: Dict = Utils.get_activity_comments()
        
        # Set the date and merge the cells
        tl_page.cell(self.current_row_position, 1, value = str(a_date.strftime(r'%A, %d %B %Y')))
        tl_page.merge_cells(start_row=self.current_row_position, start_column=1, end_column=1, end_row=total_act + self.current_row_position - 1)
        self.current_merge_row += 1

        activity: Activity
        for activity in current_day.day_activities:
            tl_page.cell(self.current_row_position, ExcelManager.START_TIME_COLUMN, self.print_in_hour_format(a_date, activity.start_time))
            tl_page.cell(self.current_row_position, ExcelManager.ACTIVITY_TIME_COLUMN, str(activity.activity_duration))
            tl_page.cell(self.current_row_position, ExcelManager.END_TIME_COLUMN, self.print_in_hour_format(a_date, activity.end_time))
            tl_page.cell(self.current_row_position, ExcelManager.ACTIVITY_NAME_COLUMN, activity.activity_name)
            tl_page.cell(self.current_row_position, ExcelManager.COMMENT_COLUMN, self.__get_activity_comment(activity.activity_name, act_comm_dict))
            self.current_row_position += 1
    
        self.current_row_position += 1 # Used to separate days with line, comment out so every day has no space in between.

    def __write_week_summary(self):
        ''' Writes the summary to the excel sheet. Even I don't really know how this works :) '''
        week_sum_sheet = self.xlfile.create_sheet(title='Summary')
        total_act_time: Dict
        c_day_list: List[Day]
        c_day_list = []
        
        # print(list(self.week_schedule))
        
        for date, day in self.week_schedule:
            c_day_list.append(day)

        total_act_time = self.get_total_time_from_activities(c_day_list)
        
        # Set the summary table labels.
        week_sum_sheet.cell(1, 1, 'Day')
        for idx, day in enumerate(Utils.DAY_PROP.keys()):
            week_sum_sheet.cell(idx + 2, 1, day)
        week_sum_sheet.cell(9, 1, 'Total Time')
        week_sum_sheet.cell(10, 1, 'Time Percentage')
        # Set column labels in summary table and fill the information
        activity_summ = ActivitySummaryContainer(c_day_list, total_act_time)
        for idx, lbl in enumerate(total_act_time.keys()):
            week_sum_sheet.cell(1, idx + 2, lbl)
            activity_summ.set_current_activity_column(lbl)
            current_day: Day
            for day_idx, current_day in enumerate(c_day_list):
                week_sum_sheet.cell(day_idx + 2, idx + 2, float(self.__from_timedelta_to_string(activity_summ.get_total_activity_time_per_day(current_day))))
                # week_sum_sheet.cell(day_idx + 2, idx + 2).number_format = '00.00 Hours'
            output_cell: excel.cell.Cell = week_sum_sheet.cell(9, idx + 2) 
            relative_cell: excel.cell.Cell =  output_cell.offset(row=-7) # Theres some weird names here...
            end_cell: excel.cell.Cell = relative_cell.offset(row=6)
            output_cell.value = r'=SUM(' + relative_cell.coordinate + r':' + end_cell.coordinate + r')'
            percentage_cell: excel.cell.Cell =  output_cell.offset(row=1)
            percentage_cell.value = '=' + output_cell.coordinate + '/' + str(ExcelManager.TOTAL_WEEK_TIME)
            percentage_cell.number_format = '00.00%'            

            activity_summ.set_null_activity_column()

        
        
    def get_total_time_of_day_activities(self, day: Day) -> Dict:
        '''This returns a dictionary with all the activities of a single day and the total duration of them'''
        day_act = {}
        act: Activity
        for act in day.day_activities:
            if act.activity_name in day_act:
                day_act[act.activity_name] += act.activity_duration
            else:
                day_act[act.activity_name] = act.activity_duration
        return day_act

    def get_total_time_from_activities(self, day_list: List[Day]):
        '''Gets the total sum of every activity during the week and returns it as a dictionary'''
        act_summary_dict = {}        
        c_day: Day
        for c_day in day_list:
            # print(c_day.day_activities)
            act: Activity
            for act in c_day.day_activities:
                # print(act.activity_name)
                if act.activity_name in act_summary_dict.keys():
                    act_summary_dict[act.activity_name] += act.activity_duration
                else:
                    act_summary_dict[act.activity_name] = timedelta(hours=0)

        return act_summary_dict
        
    
    def __from_timedelta_to_string(self, current_timedelta: timedelta) -> str:
        ''' Takes a timedeltainstance and returns a float representing total hours of the timedelta. '''
        current_seconds = current_timedelta.total_seconds()
        current_seconds = (current_seconds / 60) / 60
        return '{:.2f}'.format(current_seconds)
    
    def print_in_hour_format(self, date_given: datetime, duration: timedelta) -> str:      
        ''' Prints a timedelta as a string formated date. '''  
        to_write = date_given + duration
        return to_write.strftime('%I:%M%p')
    
    def __get_activity_comment(self, activity_name: str, act_com_dict: Dict) -> str:
        ''' Reads the comment from the resource files of a given activity. '''
        if activity_name in act_com_dict.keys():
            comments = act_com_dict[activity_name]
            if comments:
                comm = self.picker.choice(comments)                
                return comm
        else: 
            return ''
                    
    



if __name__=='__main__':
    ''' Create a new excel file, generate a complete schedule and write it to the excel file created earlier. '''
    xl = ExcelManager(input('Name for file withouth ext.\n') + '.xlsx', input('Your name.\n'))
    xl.write_schedule()
    xl.save_excel_sheet()
    




