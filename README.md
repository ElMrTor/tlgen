# **Time Log Generator**

# What is this?
- This program generates a timelog for a full week, filling it with random
and non random events according to what is given in the resource file.

# Installation and requirements
- Needs pip to install requirements or have openpyxl installed.
- To install requirements with pip on the terminal or command prompt:
- `pip install -r requirements.txt`

# Run
- Go into directory and in terminal or command prompt:
- `python excelmanager.py`

- Program will run, enter filename with no spaces and no extensions
- Then enter your name, can have as many spaces as you want.
- Then enter the date, the week starts at saturday, otherwise its gonna mess up.
- Provide a date starting with saturday. e.g. "2020 2 22" (Today)
- First enter the year, hit enter. 
> 4 digits
- Enter month number, hit enter.
> 1 or 2 digits
- Enter day number, hit enter.
> 1 or 2 digits
- Done


# Setting up
## Using the res_file.xlsx to generate stuff:
- Don't change the columns, max rows for entries is 100, if you go past that it will
be ignored and damn thats a lot of shit.

## Random Activities Section
### Activity Name
- Name of activity, just the string that's going to be used for
the activity. (Just Text)

### Minumum Duration
- Minimum time the activity can have when generating the
random activity time. Use a double, "4.00" means 4 hours, "1.20" means 1 hour
and 20 minutes, "0.02" means 2 minutes, you get the idea.

### Maximum Duration
- Minimum time the activity can have when generating the
random activity time. Same rules as minimum duration.

### Comments
- Enter comments that will be be randomly chosen for that specific
activity, separate multiple comments by using ",". 
e.g. "Why am I doing this.,I wanna watch sum hentai." -> This will separate
the string into two comments and choose one of the two that were provided.


## Day Section
### Activity Name
- Name of activity, just the string that's going to be used for
the activity. (Just Text)

### Start Time
- Enter the hour when the activity starts using the method 
previously mentioned. Use 24 hour format.
e.g. "8.45" activity starts at 8:45am, "13.05" activity starts at 1:05pm.

### Duration
- Duration of the activity. Using the same format of specifying hours
give the how much time the activity will take during the day.

